# Borgbackup

Deploy automated regular backups using borgbackup, borgmatic and systemd-timers. Manage both backup servers and hosts to backup.

Only creates and prunes repository (no checks for now)

## Requirements

Your distribution must support systemd.

Because of the lack of support of `borgmatic` in official repositories, most distribution also require `pip`.

Supported distributions:
  * CentOS 7
  * Ubuntu 18

## Quickstart

For simple backups:
  * add your server to the `borgbackup_servers` ansible group (usually in `hosts.yml`)
```
[borgbackup_servers]
server
```
  * configure your server / the `borgbackup_servers` to be a borgbackup server with a default configuration (`host_vars/<server.yml>` or `groups_vars/borgbackup_servers.yml`)
```
borgbackup_server: {}
```
  * configure your client's directories and scheduling (`host_vars/<client>.yml`)
```
borgbackup_client:
  source_directories:
    - "/etc"
  timer_options:
    - "OnUnitActiveSec=24h"
```

## Role Variables

### Installation

In some cases, it is necessary to do some preinstallation tasks. You can include then in `install/preinstall`. For exemple, to install `borgbackup` or `borgmatic` on CentOS-7 using the package manager, it is necessary to have EPEL installed. Or to use pip, you need to install it.

#### Borgbackup
```
borgbackup_install_method: package # package or binary
borgbackup_version: latest
```

By default, `borgbackup` is installed using the distribution manager if `borgbackup_package_name` is configured in `vars/<distribution>.yml` and prerequisites are set in `tasks/install/preinstall/<distributions>.yml`. In this case, the `borgbackup_version` is not considered.

Otherwise, or if you set `borgbackup_install_method` to `binary`, it will be installed in `/usr/local/bin/borg`.

#### Borgmatic

```
borgmatic_install_method: package # package or pip
borgmatic_version: latest
```

By default, `borgmatic` is installed using the distribution manager if `borgmatic_package_name` is configured in `vars/<distribution>.yml` and prerequisites are set in `tasks/install/preinstall/<distributions>.yml`. In this case, `borgmatic_version` is not considered.

Otherwise, or if you set `borgmatic_install_method` to `pip`, it will be installed in `/usr/local/bin/borgmatic`. For some distributions where pip is not installed, you need to have the `geerlingguy/ansible-role-pip` role installed.

### Servers configuration:
```
borgbackup_server:
  fqdn: # Defaults to ansible_hostname
  port: 22
  user: borg
  backups_location: ~/borgbackups
```

`fqdn` is used by the clients to access the server. Depending on your network configuration, the URL used by Ansible (`ansible_hostname`) might not be accessible from your client.

Servers can be grouped using ansible inventory groups. This allows you to target a servers group in your clients, instead of specifying multiple servers for each client.

/!\ Be careful when you set `backups_location`, because the `user` will became the owner of this folder.

### Clients configuration:

Configuration for clients can be complicated, because you can specify options globally, per client and per client's repo. This allows backup to one server for your configurations and to another for your datas for exemple.

Configuration for clients can seem complicated, but you have access to multiple depth of configuration.

#### Simple backups
In its simplest form, you listed your servers in the `borgbackup_servers` group.
```
borgbackup_client:
  source_directories:
    - "/etc"
```
Using this syntax, it will create a `default` repository. It will use the `global client configuration` (configuration stored in `borgbackup_client_default`).

/!\ If the `default` repository is defined in `borgbackup_client_repos`, this variable won't be used. Nor the variables below if they are overiden.

#### Configuration

##### Global client configuration

The global client configuration is used to provide a way to provide default configuration for repos. If it is not specified, it defaults to the following values:
```
borgbackup_client:
  servers_groups:
    - borgbackup_servers
  servers_list: # Not defined by default
    - <inventory_hostname> 
  source_directories: # Only used for the default repository
  ssh:
    key_file: "~/.ssh/borgbackup"
    key_type: "rsa"
    key_bits: "2048"
    key_comments: ""
  timer_options:
    - "OnBootSec=15min"
    - "OnUnitActiveSec=24h"
    - "RandomizedDelaySec=1h"
  borgmatic_config:
    storage:
      encryption_passphrase: # Not defined by default # Not yet supported
      archive_name_format: # Defaults to {hostname}-<repo_name>-{now}
    retention:
      - keep_daily: 14
      - keep_weekly: 6
      - keep_monthly: 10
      - keep_yearly: -1
```

#### Directories to target

To configure the list of directories to backup, you can use the `source_directories` variable for each repo. It will be merged to the `borgmatic_config: location: source_directories` list (so you can combine both methods).

For more advanced configuration, you can also directly use the `borgmatic_config: location` dictionnary. See [borgmatic configuration reference](https://torsion.org/borgmatic/docs/reference/configuration/) for more details. 

/!\ A configuration without either of those variables correctly defined is useless.

#### Servers to target

You can specify servers to target using the  `servers_groups` and `servers_list` lists.

The `servers_groups` allows you to specify a list of ansible groups.
The `servers_list` allows you to specify a list of ansible hosts.
If neither `servers_groups` nor `servers_list` are defined, `servers_groups` defaults to `borgbackup_servers`.

You can use both variables at the same time. They will both be used to generate the `borgbackup_servers` variable used in the tasks, which is a list of servers and their configurations.

Both are list for now, so you can only have one borgbackup configuration per server.

#### SSH Configuration

You can specify SSH Configuration in the `ssh` dictionnary. Usage should be self explanatory.
This will set the SSH options in the borgmatic config file.

For now, you can only have one key for all of the repositories.

/!\ Do not specify an SSH key per repository, only the global one is created.

#### Scheduling / systemd timers

You can schedule backups using the `timer_options`. It is a list of the options you desire in the [Timer] section of your systemd timers. For usage, see `man systemd.timers`.

#### Borgmatic configuration

To configure borgbackup, this role uses `borgmatic`. Any borgmatic configuration can be specified here.

Mostly you might want to configure the `source_directories` and the `retention`.

You might also want to look at `hooks` for commands to execute before, after or after errors.

/!\ If you overide the `ssh_command`, you might break something, as this is used to point to the correct `ssh_key`.

/!\ Also you shouldn't touch the `location: repositories`, they are generated from the servers configuration.

For now, if you want to prevent pruning, you need to set a `borgmatic_config: retention: keep-secondly: -1`. Either on the repo or in borgbackup_client.

### Repositories

You can specify multiple repositories in the `borgbackup_client_repos` dictionnary. This is useful to apply different configuration depending on the directories you want to backup (frequency, pruning policy).

```
borgbackup_client_repos:
  <repo-name>:
    servers_groups: # Default to borgbackup_client.servers_groups
      - <group_name>
    servers_list: # Default to borgbackup_client.servers_list
      - <inventory_hostname> 
    source_directories: # Not defined by default
    encryption: # Default to none, or authenticated if storage:encryption_passphrase is specified in borgmatic config# Not implemented
    timer_options: {{ borg_backup_default.timer_options }}
    borgmatic_config:
      #retention: {{ borg_backup_default.borgmatic_config.retention }}

      #location:
      #  source_directories:

      # hooks:
      #   before_backup:
      #   after_backup:
      #   on_error:
```

For example, the minimum repo configuration is:
```
borgbackup_client_repos:
  data-repository:
    source_directories:
      - "/etc/"
```

Any option you specify will overide default configuration for this repo.

## Example Playbook

```
- hosts: borgbackup_servers
  roles:
    - borgbackup

- hosts: all
  roles:
    - borgbackup 
```

### Security

For now, clients disable SSH host keys checking, allowing MITM attacks. SSHFP usage would be great, but is not supported on all versions of SSH. See `improvement`

Clients have full access to the server user.

Backups are made from root. Is there any security implications ?

## License

BSD

## Author Information

Made by Jonas DOREL

## Inspiration

  * https://github.com/FiaasCo/borgbackup
  * https://github.com/SphericalElephant/ansible-role-borgbackup
